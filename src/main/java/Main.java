import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        header("Aplikasi Pengolah Nilai Siswa");
        menu();

    }

    private static void menu() {

        Calculation listData = new Calculation();

        System.out.println("\nMasukkan lokasi dan nama file data (contoh: C:\\Users\\USER\\Desktop\\JAVA PROJECTS - ISA RANDRA\\Mean Median Modus\\src\\data_sekolah - data_sekolah.csv): ");
        String namaFolderAndFile=scanner.nextLine();

        int[] input = listData.readCsv(namaFolderAndFile);

        if(input==null){
            thirdSlide();
        }

        listData.setListInput(input);

        System.out.println();
        System.out.println("Pilih menu:\n" +
                "1. Generate modus.txt untuk menampilkan modus\n" +
                "2. Generate meanMedian.txt untuk menampilkan mean dan median\n" +
                "3. Generate modus.txt dan meanMedian.txt\n" +
                "4. Generate klasifikasi.txt\n" +
                "0. Exit");

        int menuUtamaControl = scanner.nextInt();

        switch (menuUtamaControl){
            case 1:
                listData.modus(input);
                menuKedua();
                break;
            case 2:
                listData.sort(input);
                listData.mean(input);
                listData.median(input);
                menuKedua();
                break;
            case 3:
                listData.sort(input);
                listData.modus(input);
                listData.mean(input);
                listData.median(input);
                menuKedua();
                break;
            case 4:
                listData.klasifikasi(input);
                menuKedua();
                break;
            case 0:
                System.exit(0);
        }
        scanner.close();
    }

    private static void thirdSlide() {
        System.out.println("\nFile tidak ditemukan, pastikan nama folder dan nama file sesuai");
        System.out.println("\nPilih menu:\n" +
                "1. Kembali ke menu utama\n" +
                "0. Exit");
        int thirdSlideControl = scanner.nextInt();
        System.out.println("------------------------------------------------------------------");
        switch (thirdSlideControl){
            case 1:
                scanner.nextLine();
                menu();
                break;
            case 0:
                scanner.close();
                System.exit(0);
        }
    }

    private static void menuKedua() {
        System.out.println("------------------------------------------------------------------");
        System.out.println("File telah digenerate di folder Mean Median Modus, silahkan cek\n");
        System.out.println("Pilih menu: \n" +
                "1. Kembali ke menu utama\n" +
                "0. Exit");

        int menuKeduaControl = scanner.nextInt();
        System.out.println("------------------------------------------------------------------");

        switch (menuKeduaControl){
            case 0:
                scanner.close();
                System.exit(0);
                break;
            case 1:
                scanner.nextLine();
                menu();
        }
        scanner.close();
    }

    private static void header(String string) {
        System.out.println("------------------------------------------------------------------");
        System.out.println(string);
        System.out.println("------------------------------------------------------------------");
    }
}
